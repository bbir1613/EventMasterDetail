package com.example.bogdan.eventmasterdetail;


import android.app.Application;
import android.util.Log;

import com.example.bogdan.eventmasterdetail.net.EventRestClient;
import com.example.bogdan.eventmasterdetail.net.EventSocketClient;
import com.example.bogdan.eventmasterdetail.service.EventManager;

import static com.example.bogdan.eventmasterdetail.content.database.EventDatabase.DATABASE_NAME;

public class EventApp extends Application {

    public static final String TAG = EventApp.class.getSimpleName();
    private EventManager mEventManager;
    private EventRestClient mEventRestClient;

    @Override
    public void onCreate() {
        Log.d(TAG, "onCreate()");
        super.onCreate();
        mEventRestClient =new EventRestClient(this);
        mEventManager = new EventManager(this);
        mEventManager.setEventSocketClient(new EventSocketClient(this));
        mEventManager.setEventRestClient(mEventRestClient);
    }

    EventManager getEeventManager() {
        return mEventManager;
    }

    @Override
    public void onTerminate() {
        Log.d(TAG, "onTerminate()");
        super.onTerminate();
    }
}
