package com.example.bogdan.eventmasterdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.content.User;
import com.example.bogdan.eventmasterdetail.util.Cancellable;
import com.example.bogdan.eventmasterdetail.util.DialogUtils;
import com.example.bogdan.eventmasterdetail.util.OnErrorListener;
import com.example.bogdan.eventmasterdetail.util.OnSuccessListener;

public class LoginActivity extends AppCompatActivity {

    public static final String TAG = LoginActivity.class.getSimpleName();
    private EventApp mEventApp;
    private Cancellable mCancellable;
    private Snackbar authenticateSnackBar;
    private Vibrator mv;
    private EditText mUsername;
    private EditText mPassword;

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mEventApp = (EventApp) getApplication();
        mv = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        mUsername = (EditText) findViewById(R.id.username);
        mPassword = (EditText) findViewById(R.id.password);

        authenticateSnackBar = Snackbar.make(findViewById(R.id.login), "Authenticating, please wait", Snackbar.LENGTH_INDEFINITE);
        Button loginButton = (Button) findViewById(R.id.login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "loginButton");
                InputMethodManager imm = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);//hide keyboard on Login click
                imm.hideSoftInputFromWindow((null == getCurrentFocus()) ? null : getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
                if (validate()) {
                    authenticateSnackBar.show();
                    login();
                }
            }
        });
    }

    private boolean validate() {
        boolean valid = true;
        if (mUsername.getText().toString().length() == 0) {
            mUsername.setError("Invalid username");
            valid = false;
        }
        if (mPassword.getText().toString().length() == 0) {
            mPassword.setError("Invalid password");
            valid = false;
        }
        if (!valid) {
            RotateAnimation ra = new RotateAnimation(0, 360, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            ra.setDuration(500);
            ra.setRepeatCount(1);
            mUsername.setAnimation(ra);
            mPassword.setAnimation(ra);
            mUsername.getAnimation().start();
            ra.setDuration(1000);
            ra.setRepeatCount(1);
            mPassword.getAnimation().start();
            vibrate();
        }
        return valid;
    }

    private void vibrate() {
        if (mv.hasVibrator()) {
            Log.d(TAG, "can vibrate");
        } else {
            Log.d(TAG, "cannot vibrate");
        }
        long[] pattern = {0, 100, 1000, 300, 200, 100, 500, 200, 100};
        mv.vibrate(pattern, -1);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mCancellable != null) {
            mCancellable.cancel();
        }
    }

    private void login() {
        mCancellable = mEventApp.getEeventManager().loginAsync(mUsername.getText().toString(),
                mPassword.getText().toString(),
                new OnSuccessListener<String>() {
                    @Override
                    public void onSuccess(final String value) {
                        runOnUiThread(new Runnable() {
                            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                            @Override
                            public void run() {
                                Log.d(TAG, "Authenticated with success " + value);
                                startEventListActivity();
                            }
                        });
                    }
                }, new OnErrorListener() {
                    @Override
                    public void onError(final Exception exception) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Log.e(TAG, exception.getMessage());
                                authenticateSnackBar.dismiss();
                                DialogUtils.showError(LoginActivity.this, exception);//current context LoginActivity.this
                            }
                        });
                    }
                });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void startEventListActivity() {
        mEventApp.getEeventManager().synchronize(new OnSuccessListener<Event>() {
            @Override
            public void onSuccess(Event value) {
                authenticateSnackBar.dismiss();
                startActivity(new Intent(LoginActivity.this, EventListActivity.class));
            }
        });
    }
}
