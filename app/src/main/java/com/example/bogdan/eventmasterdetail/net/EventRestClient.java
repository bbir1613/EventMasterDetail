package com.example.bogdan.eventmasterdetail.net;

import android.content.Context;
import android.provider.MediaStore;
import android.util.JsonReader;
import android.util.JsonWriter;
import android.util.Log;

import com.example.bogdan.eventmasterdetail.R;
import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.content.User;
import com.example.bogdan.eventmasterdetail.net.mapping.CredentialsWriter;
import com.example.bogdan.eventmasterdetail.net.mapping.EventReader;
import com.example.bogdan.eventmasterdetail.net.mapping.EventWriter;
import com.example.bogdan.eventmasterdetail.net.mapping.ResourceListReader;
import com.example.bogdan.eventmasterdetail.net.mapping.TokenReader;
import com.example.bogdan.eventmasterdetail.util.Cancellable;
import com.example.bogdan.eventmasterdetail.util.OnErrorListener;
import com.example.bogdan.eventmasterdetail.util.OnSuccessListener;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.List;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class EventRestClient {

    public static final String UTF_8 = "UTF-8";
    public static final String TAG = EventRestClient.class.getSimpleName();
    public static final String APPLICATION_JSON = "application/json";
    public static final String LAST_MODIFIED = "Last-Modified";

    private final OkHttpClient mOkHttpClient;
    private final String mApiUrl;
    private final String mEventUrl;
    private final String mAuthUrl;
    private User mUser;

    public EventRestClient(Context context) {
        mOkHttpClient = new OkHttpClient();
        mApiUrl = context.getString(R.string.api_url);
        mEventUrl = mApiUrl.concat("/api/event");
        mAuthUrl = mApiUrl.concat("/api/auth");
    }

    public void setUser(User mUser) {
        this.mUser = mUser;
    }

    public Cancellable searchAsync(String mEventsLastUpdate,
                                   final OnSuccessListener<LastModifiedList<Event>> onSuccessListener,
                                   final OnErrorListener onErrorListener) {
        Request.Builder requestBuilder = new Request.Builder().url(mEventUrl);
        if (mEventsLastUpdate != null) {
            requestBuilder.header(LAST_MODIFIED, mEventsLastUpdate);
        }
        addAuthToken(requestBuilder);
        return new CancellableOkHttpAsync<LastModifiedList<Event>>(
                requestBuilder.build(),
                new ResponseReader<LastModifiedList<Event>>() {
                    @Override
                    public LastModifiedList<Event> read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        return new LastModifiedList<Event>(
                                response.header(LAST_MODIFIED),
                                new ResourceListReader<Event>(new EventReader()).read(reader)
                        );
                    }
                },
                onSuccessListener,
                onErrorListener
        );
    }

    public Cancellable readAsync(String eventId,
                                 final OnSuccessListener<Event> onSuccessListener,
                                 final OnErrorListener onErrorListener) {
        Request.Builder builder = new Request.Builder().url(String.format("%s/%s", mEventUrl, eventId));
        addAuthToken(builder);
        return new CancellableOkHttpAsync<>(
                builder.build(),
                new ResponseReader<Event>() {
                    @Override
                    public Event read(Response response) throws Exception {
                        if (response.code() == 200) {
                            JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                            return new EventReader().read(reader);
                        }
                        return null;
                    }
                },
                onSuccessListener,
                onErrorListener
        );
    }

    public Cancellable deleteAsync(String eventId,
                                   final OnSuccessListener<Event> onSuccessListener,
                                   final OnErrorListener onErrorListener) {
        Request.Builder builder = new Request.Builder().url(String.format("%s/%s", mEventUrl, eventId));
        addAuthToken(builder);
        builder.delete();
        return new CancellableOkHttpAsync<>(
                builder.build(),
                new ResponseReader<Event>() {
                    @Override
                    public Event read(Response response) throws Exception {
                        if (response.code() == 200) {
                            JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                            return new EventReader().read(reader);
                        }
                        return null;
                    }
                },
                onSuccessListener,
                onErrorListener
        );
    }

    private void addAuthToken(Request.Builder requestBuilder) {
        User user = mUser;
        if (user != null) {
            requestBuilder.header("Authorization", String.format("Bearer %s", user.getToken()));
        }
    }

    public CancellableOkHttpAsync<String> getToken(User user,
                                                   OnSuccessListener<String> successListener,
                                                   OnErrorListener errorListener) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        JsonWriter writer = null;
        try {
            writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new CredentialsWriter().write(user, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, "getToken failed ", e);
            throw new ResourceException(e);
        }
        return new CancellableOkHttpAsync<>(
                new Request.Builder()
                        .url(String.format("%s/session", mAuthUrl))
                        .post(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()))
                        .build(),
                new ResponseReader<String>() {

                    @Override
                    public String read(Response response) throws Exception {
                        JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                        if (response.code() == 201) {
                            return new TokenReader().read(reader);
                        } else {
                            return null;
                        }
                    }
                },
                successListener,
                errorListener
        );
    }

    public Cancellable updateAsync(final Event event,
                                   final OnSuccessListener<Event> onSuccessListener,
                                   final OnErrorListener onErrorListener) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new EventWriter().write(event, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, "updateAsync failed", e);
            onErrorListener.onError(new ResourceException(e));
        } finally {
            Request.Builder builder = new Request.Builder()
                    .url(String.format("%s/%s", mEventUrl, event.getId()))
                    .put(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()));
            addAuthToken(builder);
            return new CancellableOkHttpAsync<Event>(builder.build(), new ResponseReader<Event>() {
                @Override
                public Event read(Response response) throws Exception {
                    int code = response.code();
                    JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                    if (code == 400 || code == 409 || code == 405) { //bad request, conflict, method not allowed
//                        throw new ResourceException(new ResourceListReader<Issue>(new IssueReader()).read(reader));
                        throw new ResourceException(new RuntimeException("error"));
                    }
                    return new EventReader().read(reader);
                }
            }, onSuccessListener, onErrorListener);
        }
    }

    public Cancellable saveAsync(final Event event,
                                 final OnSuccessListener<Event> onSuccessListener,
                                 final OnErrorListener onErrorListener) {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            JsonWriter writer = new JsonWriter(new OutputStreamWriter(baos, UTF_8));
            new EventWriter().write(event, writer);
            writer.close();
        } catch (Exception e) {
            Log.e(TAG, "updateAsync failed", e);
            onErrorListener.onError(new ResourceException(e));
        } finally {
            Request.Builder builder = new Request.Builder()
                    .url(mEventUrl)
                    .post(RequestBody.create(MediaType.parse(APPLICATION_JSON), baos.toByteArray()));
            addAuthToken(builder);
            return new CancellableOkHttpAsync<Event>(builder.build(), new ResponseReader<Event>() {
                @Override
                public Event read(Response response) throws Exception {
                    int code = response.code();
                    JsonReader reader = new JsonReader(new InputStreamReader(response.body().byteStream(), UTF_8));
                    if (code == 400 || code == 409 || code == 405) { //bad request, conflict, method not allowed
//                        throw new ResourceException(new ResourceListReader<Issue>(new IssueReader()).read(reader));
                        throw new ResourceException(new RuntimeException("error"));
                    }
                    return new EventReader().read(reader);
                }
            }, onSuccessListener, onErrorListener);
        }
    }

    private static interface ResponseReader<E> {
        E read(Response response) throws Exception;
    }

    private class CancellableOkHttpAsync<T> implements Cancellable {
        private Call mCall;

        public CancellableOkHttpAsync(final Request request,
                                      final ResponseReader<T> responseReader,
                                      final OnSuccessListener<T> successListener,
                                      final OnErrorListener errorListener) {
            try {
                mCall = mOkHttpClient.newCall(request);
                Log.d(TAG, String.format("started %s %s", request.method(), request.url()));
                mCall.enqueue(new Callback() {
                    @Override
                    public void onFailure(Call call, IOException e) {
                        notifyFailure(e, request, errorListener);
                    }

                    @Override
                    public void onResponse(Call call, Response response) throws IOException {
                        try {
                            notifySuccess(response, request, successListener, responseReader);
                        } catch (Exception e) {
                            notifyFailure(e, request, errorListener);
                        }
                    }
                });
            } catch (Exception e) {
                notifyFailure(e, request, errorListener);
            }
        }


        @Override
        public void cancel() {
            if (mCall != null) {
                mCall.cancel();
            }
        }

        private void notifySuccess(Response response, Request request,
                                   OnSuccessListener<T> successListener, ResponseReader<T> responseReader) throws Exception {
            if (mCall.isCanceled()) {
                Log.d(TAG, String.format("completed, but cancelled %s %s", request.method(), request.url()));
            } else {
                Log.d(TAG, String.format("completed %s %s", request.method(), request.url()));
                successListener.onSuccess(responseReader.read(response));
            }
        }

        private void notifyFailure(Exception e, Request request, OnErrorListener errorListener) {
            if (mCall.isCanceled()) {
                Log.d(TAG, String.format("failed, but cancelled %s %s", request.method(), request.url()));
            } else {
                Log.e(TAG, String.format("failed %s %s", request.method(), request.url()), e);
                errorListener.onError(e instanceof ResourceException ? e : new ResourceException(e));
            }
        }
    }
}
