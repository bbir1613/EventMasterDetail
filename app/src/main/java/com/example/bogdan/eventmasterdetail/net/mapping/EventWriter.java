package com.example.bogdan.eventmasterdetail.net.mapping;

import android.util.JsonWriter;

import com.example.bogdan.eventmasterdetail.content.Event;

import java.io.IOException;

import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.TEXT;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.VERSION;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event._ID;

public class EventWriter implements ResourceWriter<Event, JsonWriter> {
    @Override
    public void write(Event event, JsonWriter jsonWriter) throws IOException {
        jsonWriter.beginObject();
        {
            if (event.getId() != null) {
                jsonWriter.name(_ID).value(event.getId());
            }
            jsonWriter.name(TEXT).value(event.getText());
            jsonWriter.name(VERSION).value(event.getVersion());
        }
        jsonWriter.endObject();
    }
}
