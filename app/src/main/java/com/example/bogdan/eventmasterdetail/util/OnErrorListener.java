package com.example.bogdan.eventmasterdetail.util;

public interface OnErrorListener {
    void onError(Exception exception);
}
