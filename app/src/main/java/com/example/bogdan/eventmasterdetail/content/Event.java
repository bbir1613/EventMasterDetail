package com.example.bogdan.eventmasterdetail.content;

public class Event {
    //TODO(Bogdan): add status
    public enum Status {
        active,
        done;
    }

    private String mId;
    private String mUserId;
    private String mText;
    private String mWhat = "";
    private Status mStatus = Status.active;
    private long mUpdated;
    private int mVersion;

    public Event() {
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getUserId() {
        return mUserId;
    }

    public void setUserId(String mUserId) {
        this.mUserId = mUserId;
    }

    public String getText() {
        return mText;
    }

    public void setText(String mText) {
        this.mText = mText;
    }

    public Status getStatus() {
        return mStatus;
    }

    public void setStatus(Status mStatus) {
        this.mStatus = mStatus;
    }

    public long getUpdated() {
        return mUpdated;
    }

    public void setUpdated(long mUpdated) {
        this.mUpdated = mUpdated;
    }

    public int getVersion() {
        return mVersion;
    }

    public void setVersion(int mVersion) {
        this.mVersion = mVersion;
    }

    public String getWhat() {
        return mWhat;
    }

    public void setWhat(String mWhat) {
        this.mWhat = mWhat;
    }

    @Override
    public String toString() {
        return "Event{" +
                "mId='" + mId + '\'' +
                ", mUserId='" + mUserId + '\'' +
                ", mText='" + mText + '\'' +
                ", mStatus=" + mStatus +
                ", mUpdated=" + mUpdated +
                ", mVersion=" + mVersion +
                ", mWhat=" + mWhat +
                '}';
    }
}
