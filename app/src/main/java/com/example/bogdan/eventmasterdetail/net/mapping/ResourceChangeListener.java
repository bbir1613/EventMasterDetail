package com.example.bogdan.eventmasterdetail.net.mapping;

public interface ResourceChangeListener<E> {
    void onCreated(E e);
    void onUpdated(E e);
    void onDeleted(String id);
    void onError(Throwable t);

}
