package com.example.bogdan.eventmasterdetail;

import android.app.Activity;
import android.content.Context;
import android.support.design.widget.CollapsingToolbarLayout;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.util.Cancellable;
import com.example.bogdan.eventmasterdetail.util.DialogUtils;
import com.example.bogdan.eventmasterdetail.util.OnErrorListener;
import com.example.bogdan.eventmasterdetail.util.OnSuccessListener;

/**
 * A fragment representing a single Item detail screen.
 * This fragment is either contained in a {@link EventListActivity}
 * in two-pane mode (on tablets) or a {@link EventDetailActivity}
 * on handsets.
 */
public class EventDetailFragment extends Fragment {
    public static final String TAG = EventDetailFragment.class.getSimpleName();

    /**
     * The fragment argument representing the item ID that this fragment
     * represents.
     */
    public static final String EVENT_ID = "event_id";

    /**
     * The dummy content this fragment is presenting.
     */
    private Event mEvent;
    private EventApp mEventApp;
    private CollapsingToolbarLayout mAppBarLayout;
    private TextView mEventTextView;
    private Cancellable mFetchEvent;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public EventDetailFragment() {
    }

    @Override
    public void onAttach(Context context) {
        Log.d(TAG, "onAttach");
        super.onAttach(context);
        mEventApp = (EventApp) context.getApplicationContext();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG, "onCreate");
        if (getArguments().containsKey(EVENT_ID)) {
            Activity activity = this.getActivity();
            mAppBarLayout = (CollapsingToolbarLayout) activity.findViewById(R.id.toolbar_layout);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView");
        View rootView = inflater.inflate(R.layout.item_detail, container, false);
        mEventTextView = (TextView) rootView.findViewById(R.id.item_detail);
        fillEventDetails();
        fetchEventAsync();
        return rootView;
    }

    @Override
    public void onStop() {
        super.onStop();
        mFetchEvent.cancel();
    }

    private void fetchEventAsync() {
        mFetchEvent = mEventApp.getEeventManager().getEventAsync(
                getArguments().getString(EVENT_ID),
                new OnSuccessListener<Event>() {

                    @Override
                    public void onSuccess(final Event event) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                mEvent = event;
                                fillEventDetails();
                            }
                        });
                    }
                },
                new OnErrorListener() {
                    @Override
                    public void onError(final Exception exception) {
                        getActivity().runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                DialogUtils.showError(getActivity(), exception);
                            }
                        });
                    }
                }
        );
    }

    private void fillEventDetails() {
        if (mEvent != null) {
            if (mAppBarLayout != null) {
                mAppBarLayout.setTitle(mEvent.getText());
            }
            mEventTextView.setText(mEvent.getText());
        }
    }
}
