package com.example.bogdan.eventmasterdetail;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.design.widget.FloatingActionButton;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.util.Cancellable;
import com.example.bogdan.eventmasterdetail.util.DialogUtils;
import com.example.bogdan.eventmasterdetail.util.OnErrorListener;
import com.example.bogdan.eventmasterdetail.util.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;

/**
 * An activity representing a list of Items. This activity
 * has different presentations for handset and tablet-size devices. On
 * handsets, the activity presents a list of items, which when touched,
 * lead to a {@link EventDetailActivity} representing
 * item details. On tablets, the activity presents the list of items and
 * item details side-by-side using two vertical panes.
 */
public class EventListActivity extends AppCompatActivity {
    public static final String TAG = EventListActivity.class.getSimpleName();

    /**
     * Whether or not the activity is in two-pane mode, i.e. running on a tablet
     * device.
     */
    private boolean mTwoPane;
    private RecyclerView mRecyclerView;
    private EventApp mEventApp;
    private boolean mEventsLoaded;
    private Cancellable mCancellable;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_list);
        mEventApp = (EventApp) getApplication();
        setupToolbar();
        setupFloatingActionButton();
        setupRecyclerView();
        checkTwoPaneMode();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    protected void onStart() {
        super.onStart();
        startGetEventsAsyncCall();
        mEventApp.getEeventManager().subscribeChangeListener();
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCancellable.cancel();
        mEventApp.getEeventManager().unsubscribeChangeListener();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    private void startGetEventsAsyncCall() {
        Log.d(TAG, "startGetEventsAsyncCall()");
        if (mEventsLoaded) {
            Log.d(TAG, "start startGetEventsAsyncCall - content already loaded, return");
            return;
        }
        mCancellable = mEventApp.getEeventManager().getEventsAsync(
                new OnSuccessListener<List<Event>>() {

                    @Override
                    public void onSuccess(final List<Event> events) {
                        Log.d(TAG, "getEventsAsync - success");
//                        mEventsLoaded = true;
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showContent(events);
                            }
                        });
                    }
                },
                new OnErrorListener() {

                    @Override
                    public void onError(final Exception exception) {
                        Log.d(TAG, "getEventsAsync - error");
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showError(exception);
                            }
                        });
                    }
                }
        );
    }

    private void showError(Exception exception) {
        DialogUtils.showError(EventListActivity.this, exception);
    }

    private void showContent(List<Event> events) {
        Log.d(TAG, "showContent()");
        mRecyclerView.setAdapter(new EventRecyclerViewAdapter(events));
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    private void setupRecyclerView() {
        mRecyclerView = (RecyclerView) findViewById(R.id.item_list);
        mRecyclerView.setVisibility(View.GONE);
        registerForContextMenu(mRecyclerView);
    }

    private void checkTwoPaneMode() {
        if (findViewById(R.id.item_detail_container) != null) {
            // The detail container view will be present only in the
            // large-screen layouts (res/values-w900dp).
            // If this view is present, then the
            // activity should be in two-pane mode.
            mTwoPane = true;
        }
    }

    private void setupFloatingActionButton() {
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(view.getContext(), EventAddActivity.class);
                intent.putExtra(EventAddActivity.TITLE, R.string.add_event);
                view.getContext().startActivity(intent);
            }
        });
    }

    private void setupToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(getTitle());
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Event event = ((EventRecyclerViewAdapter) mRecyclerView.getAdapter()).getSelectedEvent();
        switch (item.getItemId()) {
            case R.string.update:
                Log.d(TAG, "update event with id : " + event.getId());
                Intent intent = new Intent(this, EventAddActivity.class);
                intent.putExtra(EventAddActivity.TITLE, R.string.update_event);
                intent.putExtra(EventAddActivity.ID, event.getId());
                intent.putExtra(EventAddActivity.TEXT, event.getText());
                intent.putExtra(EventAddActivity.VERSION, event.getVersion());
                this.startActivity(intent);
                break;
            case R.string.delete:
                Log.d(TAG, "delete event with id :" + event.getId());
                mCancellable = mEventApp.getEeventManager().deleteEventAsync(event.getId(), new OnSuccessListener<List<Event>>() {
                    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
                    @Override
                    public void onSuccess(List<Event> events) {
                        Log.d(TAG, " delete event succeeded");
                        startGetEventsAsyncCall();//TODO(BOGDAN):remove this
//                        ((EventRecyclerViewAdapter) mRecyclerView.getAdapter()).setValues(events);
//                        mRecyclerView.getAdapter().notifyDataSetChanged();
                    }
                }, new OnErrorListener() {
                    @Override
                    public void onError(final Exception exception) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                showError(exception);
                            }
                        });
                    }
                });
                break;
            default:
                Log.d(TAG, "option " + item.getItemId() + " is not implement yet");
        }
        return super.onContextItemSelected(item);
    }

    public class EventRecyclerViewAdapter
            extends RecyclerView.Adapter<EventRecyclerViewAdapter.ViewHolder> {

        private List<Event> mValues;
        private Event selectedEvent;

        public Event getSelectedEvent() {
            return selectedEvent;
        }

        public EventRecyclerViewAdapter() {
            mValues = new ArrayList<>();
        }

        public void setValues(List<Event> items) {
            mValues = items;
        }

        public EventRecyclerViewAdapter(List<Event> items) {
            mValues = items;
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.item_list_content, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            holder.mItem = mValues.get(position);
            holder.mIdView.setText(mValues.get(position).getId());
            holder.mContentView.setText(mValues.get(position).getText());

            holder.mView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mTwoPane) {
                        Bundle arguments = new Bundle();
                        arguments.putString(EventDetailFragment.EVENT_ID, holder.mItem.getId());
                        EventDetailFragment fragment = new EventDetailFragment();
                        fragment.setArguments(arguments);
                        getSupportFragmentManager().beginTransaction()
                                .replace(R.id.item_detail_container, fragment)
                                .commit();
                    } else {
                        Context context = v.getContext();
                        Intent intent = new Intent(context, EventDetailActivity.class);
                        intent.putExtra(EventDetailFragment.EVENT_ID, holder.mItem.getId());

                        context.startActivity(intent);
                    }
                }
            });

            holder.mView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    selectedEvent = holder.mItem;
                    return false;
                }
            });
        }

        @Override
        public int getItemCount() {
            return mValues.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
            public final View mView;
            public final TextView mIdView;
            public final TextView mContentView;
            public Event mItem;

            public ViewHolder(View view) {
                super(view);
                mView = view;
                mIdView = (TextView) view.findViewById(R.id.id);
                mContentView = (TextView) view.findViewById(R.id.content);
                view.setOnCreateContextMenuListener(this);
            }

            @Override
            public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
                menu.setHeaderTitle("Select the action");
                menu.add(Menu.NONE, R.string.update, Menu.NONE, R.string.update);
                menu.add(Menu.NONE, R.string.delete, Menu.NONE, R.string.delete);
            }

            @Override
            public String toString() {
                return super.toString() + " '" + mContentView.getText() + "'";
            }
        }
    }
}
