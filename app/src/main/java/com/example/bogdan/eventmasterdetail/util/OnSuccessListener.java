package com.example.bogdan.eventmasterdetail.util;

public interface OnSuccessListener<T> {
    void onSuccess(T value);
}
