package com.example.bogdan.eventmasterdetail.net.mapping;

public class Api {
    public static class Event {
        public static final String EVENT_CREATED = "event/created";
        public static final String EVENT_UPDATED = "event/updated";
        public static final String EVENT_DELETED = "event/deleted";

        public static final String _ID = "_id";
        public static final String TEXT = "text";
        public static final String STATUS = "status";
        public static final String UPDATED = "updated";
        public static final String USER_ID = "user";
        public static final String VERSION = "version";
    }

    public static class Auth {
        public static final String TOKEN = "token";
        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
    }
}
