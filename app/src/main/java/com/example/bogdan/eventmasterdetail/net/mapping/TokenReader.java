package com.example.bogdan.eventmasterdetail.net.mapping;


import android.util.JsonReader;

import org.json.JSONException;

import java.io.IOException;

import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Auth.TOKEN;

public class TokenReader implements ResourceReader<String, JsonReader> {

    @Override
    public String read(JsonReader jsonReader) throws IOException, JSONException, Exception {
        jsonReader.beginObject();
        String token = null;
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            if (name.equals(TOKEN)) {
                token = jsonReader.nextString();
            }
        }
        jsonReader.endObject();
        return token;
    }
}
