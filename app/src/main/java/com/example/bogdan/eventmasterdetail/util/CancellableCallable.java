package com.example.bogdan.eventmasterdetail.util;

import java.util.concurrent.Callable;

public interface CancellableCallable<T> extends Callable<T>, Cancellable {
}
