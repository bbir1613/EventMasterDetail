package com.example.bogdan.eventmasterdetail.net.mapping;

import android.content.Context;
import android.util.Log;

import com.example.bogdan.eventmasterdetail.R;
import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.net.ResourceException;


import org.json.JSONObject;

import io.socket.client.IO;
import io.socket.client.Socket;
import io.socket.emitter.Emitter;

import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.EVENT_CREATED;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.EVENT_DELETED;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.EVENT_UPDATED;

public class EventSocketClient {
    private static final String TAG = EventSocketClient.class.getSimpleName();
    private final Context mContext;
    private Socket mSocket;
    private ResourceChangeListener<Event> mResourceListener;

    public EventSocketClient(Context context) {
        this.mContext = context;
        Log.d(TAG, "created");
    }

    public void subscribe(final ResourceChangeListener<Event> resourceListener) {
        Log.d(TAG, "subscribe");
        mResourceListener = resourceListener;
        try {
            mSocket = IO.socket(mContext.getString(R.string.api_url));
            mSocket.on(Socket.EVENT_CONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "socket connected");
                }
            });
            mSocket.on(Socket.EVENT_DISCONNECT, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    Log.d(TAG, "socket disconnected");
                }
            });
            mSocket.on(EVENT_CREATED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        Event event = new EventJsonObjectReader().read((JSONObject) args[0]);
                        Log.d(TAG, String.format("note created %s", event.toString()));
                        mResourceListener.onCreated(event);
                    } catch (Exception e) {
                        Log.w(TAG, "event created", e);
                        mResourceListener.onError(new ResourceException(e));
                    }
                }
            });
            mSocket.on(EVENT_UPDATED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        Event event = new EventJsonObjectReader().read((JSONObject) args[0]);
                        Log.d(TAG, String.format("note updated %s", event.toString()));
                        mResourceListener.onUpdated(event);
                    } catch (Exception e) {
                        Log.w(TAG, "event updated", e);
                        mResourceListener.onError(new ResourceException(e));
                    }
                }
            });
            mSocket.on(EVENT_DELETED, new Emitter.Listener() {
                @Override
                public void call(Object... args) {
                    try {
                        String id = new IdJsonObjectReader().read((JSONObject) args[0]);
                        Log.d(TAG, String.format("note deleted %s", id));
                        mResourceListener.onDeleted(id);
                    } catch (Exception e) {
                        Log.w(TAG, "event deleted", e);
                        mResourceListener.onError(new ResourceException(e));
                    }
                }
            });
            mSocket.connect();
        } catch (Exception e) {
            Log.w(TAG, "socket error", e);
            mResourceListener.onError(new ResourceException(e));
        }
    }
}
