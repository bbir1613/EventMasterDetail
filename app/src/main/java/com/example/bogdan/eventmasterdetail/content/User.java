package com.example.bogdan.eventmasterdetail.content;


public class User {
    private String username;
    private String password;
    private String token;

    public User(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public boolean equals(Object u) {
        User user = (User) u;
        return username.equals(user.username) && password.equals(user.password);
    }

    @Override
    public String toString() {
        return username + " " + password;
    }
}
