package com.example.bogdan.eventmasterdetail.net.mapping;

import android.util.JsonReader;
import android.util.Log;

import com.example.bogdan.eventmasterdetail.content.Event;

import org.json.JSONException;

import java.io.IOException;

import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.STATUS;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.TEXT;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.UPDATED;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.USER_ID;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.VERSION;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event._ID;

public class EventReader implements ResourceReader<Event, JsonReader> {
    private static final String TAG = EventReader.class.getSimpleName();


    @Override
    public Event read(JsonReader jsonReader) throws IOException, JSONException, Exception {
        Event event = new Event();
        jsonReader.beginObject();
        while (jsonReader.hasNext()) {
            String name = jsonReader.nextName();
            switch (name) {
                case _ID:
                    event.setId(jsonReader.nextString());
                    break;
                case TEXT:
                    event.setText(jsonReader.nextString());
                    break;
                case STATUS:
                    event.setStatus(Event.Status.valueOf(jsonReader.nextString()));
                    break;
                case UPDATED:
                    event.setUpdated(jsonReader.nextLong());
                    break;
                case USER_ID:
                    event.setUserId(jsonReader.nextString());
                    break;
                case VERSION:
                    event.setVersion(jsonReader.nextInt());
                    break;
                default:
                    jsonReader.skipValue();
                    Log.w(TAG, String.format("Event property '%s' ignored ", name));
            }
        }
        jsonReader.endObject();
        Log.d(TAG, "return event " + event.getId());
        return event;
    }
}
