package com.example.bogdan.eventmasterdetail;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.util.DialogUtils;
import com.example.bogdan.eventmasterdetail.util.OnErrorListener;
import com.example.bogdan.eventmasterdetail.util.OnSuccessListener;

public class EventAddActivity extends AppCompatActivity {
    public static final String TITLE = "TITLE";
    public static final String POSITION = "POSITION";
    public static final String ID = "ID";
    public static final String TEXT = "TEXT";
    public static final String VERSION = "VERSION";
    public static final String TAG = EventAddActivity.class.getSimpleName();
    private TextView mTitle;
    private Snackbar snackbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event_add);
        mTitle = (TextView) findViewById(R.id.event_title);
        setUpToolbar();
        snackbar = Snackbar.make(findViewById(R.id.event_add_update_button), "Saving, please wait .. ", Snackbar.LENGTH_INDEFINITE);
        final EventApp mEventApp = (EventApp) getApplication();
        setUpButton(mEventApp);
    }

    private void setUpButton(final EventApp mEventApp) {
        Button addOrUpdateEvent = (Button) findViewById(R.id.event_add_update_button);
        int title = getIntent().getIntExtra(TITLE, -1);
        switch (title) {
            case R.string.add_event:
                Log.d(TAG, " button add click listener");
                addOrUpdateEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.show();
                        String eventText = String.valueOf(mTitle.getText());
                        Event event = new Event();
                        event.setText(eventText);
                        Log.d(TAG, eventText);
                        mEventApp.getEeventManager().saveEventAsync(event, new OnSuccessListener<Event>() {
                            @Override
                            public void onSuccess(Event value) {
                                if (value != null) {
                                    Log.d(TAG, value.toString());
                                } else {
                                    Log.d(TAG, "is null");
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        snackbar.dismiss();
                                        finish();
                                    }
                                });
                            }
                        }, new OnErrorListener() {
                            @Override
                            public void onError(final Exception exception) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        snackbar.dismiss();
                                        DialogUtils.showError(EventAddActivity.this, exception);
                                    }
                                });
                            }
                        });
                    }
                });
                break;
            case R.string.update_event:
                Log.d(TAG, " button update click listener");
                addOrUpdateEvent.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        snackbar.show();
                        String eventText = String.valueOf(mTitle.getText());
                        Event event = new Event();
                        event.setText(eventText);
                        event.setId(getIntent().getStringExtra(ID));
                        event.setVersion(getIntent().getIntExtra(VERSION, -1));
                        Log.d(TAG, " Update event " + event.toString());
                        mEventApp.getEeventManager().updateEventAsync(event, new OnSuccessListener<Event>() {
                            @Override
                            public void onSuccess(Event value) {
                                if (value != null) {
                                    Log.d(TAG, value.toString());
                                } else {
                                    Log.d(TAG, "is null");
                                }
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        snackbar.dismiss();
                                        finish();
                                    }
                                });
                            }
                        }, new OnErrorListener() {
                            @Override
                            public void onError(final Exception exception) {
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        snackbar.dismiss();
                                        DialogUtils.showError(EventAddActivity.this, exception);
                                    }
                                });
                            }
                        });
                    }
                });
                break;
            default:
                Log.d(TAG, "no match for title ");
                break;
        }
    }

    private void setUpToolbar() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        int title = getIntent().getIntExtra(TITLE, -1);
        switch (title) {
            case R.string.add_event:
                Log.d(TAG, "set title with value " + R.string.add_event);
                toolbar.setTitle(R.string.add_event);
                break;
            case R.string.update_event:
                Log.d(TAG, "set title with value " + R.string.update_event);
                toolbar.setTitle(R.string.update_event);
                mTitle.setText(getIntent().getStringExtra(TEXT));
                break;
            default:
                Log.d(TAG, "no match for title ");
                break;
        }
        setSupportActionBar(toolbar);
    }

}
