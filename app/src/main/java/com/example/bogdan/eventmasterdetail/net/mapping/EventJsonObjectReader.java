package com.example.bogdan.eventmasterdetail.net.mapping;

import com.example.bogdan.eventmasterdetail.content.Event;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.TEXT;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.UPDATED;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.USER_ID;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event.VERSION;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event._ID;

public class EventJsonObjectReader implements ResourceReader<Event, JSONObject> {
    private static final String TAG = EventJsonObjectReader.class.getSimpleName();

    @Override
    public Event read(JSONObject jsonObject) throws IOException, JSONException, Exception {
        Event event = new Event();
        event.setId(jsonObject.getString(_ID));
        event.setText(jsonObject.getString(TEXT));
        event.setUpdated(jsonObject.getLong(UPDATED));
        event.setUserId(jsonObject.getString(USER_ID));
        event.setVersion(jsonObject.getInt(VERSION));
        return event;
    }
}
