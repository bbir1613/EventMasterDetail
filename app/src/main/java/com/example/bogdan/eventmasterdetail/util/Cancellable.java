package com.example.bogdan.eventmasterdetail.util;


public interface Cancellable {
    void cancel();
}
