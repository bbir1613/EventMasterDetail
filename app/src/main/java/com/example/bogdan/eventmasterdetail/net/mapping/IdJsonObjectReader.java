package com.example.bogdan.eventmasterdetail.net.mapping;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Event._ID;

public class IdJsonObjectReader implements  ResourceReader<String, JSONObject>{
    @Override
    public String read(JSONObject jsonObject) throws IOException, JSONException, Exception {
        return jsonObject.getString(_ID);
    }
}
