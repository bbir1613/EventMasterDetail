package com.example.bogdan.eventmasterdetail.service;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;

import com.example.bogdan.eventmasterdetail.EventApp;
import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.content.User;
import com.example.bogdan.eventmasterdetail.content.database.EventDatabase;
import com.example.bogdan.eventmasterdetail.net.EventRestClient;
import com.example.bogdan.eventmasterdetail.net.EventSocketClient;
import com.example.bogdan.eventmasterdetail.net.LastModifiedList;
import com.example.bogdan.eventmasterdetail.net.ResourceException;
import com.example.bogdan.eventmasterdetail.net.mapping.ResourceChangeListener;
import com.example.bogdan.eventmasterdetail.util.Cancellable;
import com.example.bogdan.eventmasterdetail.util.OnErrorListener;
import com.example.bogdan.eventmasterdetail.util.OnSuccessListener;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class EventManager {

    private static final String TAG = EventManager.class.getSimpleName();
    private Context mContext;
    private EventDatabase mEventDatabase;
    private EventRestClient mEventRestClient;
    private EventSocketClient mEventSocketClient;
    private String mToken;
    private String mEventLastUpdate;
    private User mCurrentUser;
    private ConcurrentMap<String, Event> mEvents = new ConcurrentHashMap<String, Event>();

    public EventManager(EventRestClient eventRestClient) {
        mEventRestClient = eventRestClient;
    }

    public EventManager(Context context) {
        this.mContext = context;
        this.mEventDatabase = new EventDatabase(context);
    }

    public void setEventRestClient(EventRestClient eventRestClient) {
        this.mEventRestClient = eventRestClient;
    }

    public Cancellable getEventAsync(final String eventId,
                                     final OnSuccessListener<Event> onSuccessListener,
                                     final OnErrorListener onErrorListener) {
        Log.d(TAG, "getEventAsync...");
        return mEventRestClient.readAsync(eventId, new OnSuccessListener<Event>() {

            @Override
            public void onSuccess(Event event) {
                Log.d(TAG, "getEventAsync succeeded");
                onSuccessListener.onSuccess(event);
            }
        }, onErrorListener);
    }

    public Cancellable deleteEventAsync(final String eventId,
                                        final OnSuccessListener<List<Event>> onSuccessListener,
                                        final OnErrorListener onErrorListener) {
        return mEventRestClient.deleteAsync(eventId, new OnSuccessListener<Event>() {
            @Override
            public void onSuccess(Event value) {
                Log.d(TAG, "deleteAsync succeeded" + value);
                onSuccessListener.onSuccess(new ArrayList<Event>());
            }
        }, new OnErrorListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onError(Exception exception) {
                Event event = mEventDatabase.getEvent(eventId);
                event.setWhat(EventDatabase.DELETE);
                mEventDatabase.saveEvent(event);
                onSuccessListener.onSuccess(new ArrayList<Event>());
//                onErrorListener.onError(exception);
            }
        });
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public Cancellable getEventsAsync(final OnSuccessListener<List<Event>> onSuccessListener, final OnErrorListener onErrorListener) {
        Log.d(TAG, "getEventsAsync...");
        List<Event> e = mEventDatabase.getEvents();
        if (e != null) {
            onSuccessListener.onSuccess(e == null ? new ArrayList<Event>() : e);
        }
        return mEventRestClient.searchAsync(mEventLastUpdate, new OnSuccessListener<LastModifiedList<Event>>() {
            @Override
            public void onSuccess(LastModifiedList<Event> result) {
                Log.d(TAG, "searchAsync");
                List<Event> events = result.getList();
                if (events != null) {
                    mEventDatabase.saveEvents(events);
                    mEventLastUpdate = result.getLastModified();
                }
                onSuccessListener.onSuccess(events);
            }
        }, new OnErrorListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onError(Exception exception) {
                try {
                    List<Event> events = mEventDatabase.getEvents();
                    if (events != null) {
                        onSuccessListener.onSuccess(events);
                    } else {
                        onErrorListener.onError(exception);
                    }
                } catch (Exception ex) {
                    onErrorListener.onError(ex);
                }
            }
        });
    }

    public Cancellable loginAsync(final String username, final String password,
                                  final OnSuccessListener<String> successListener,
                                  final OnErrorListener errorListener) {
        Log.d(TAG, "loginAsync()");
        final User user = new User(username, password);
        return mEventRestClient.getToken(user, new OnSuccessListener<String>() {
            @Override
            public void onSuccess(String token) {
                mToken = token;
                if (token != null) {
                    user.setToken(mToken);
                    user.setPassword(password);
                    user.setUsername(username);
                    setCurrentUser(user);
                    mEventDatabase.saveUser(user);
                    successListener.onSuccess(mToken);
                } else {
                    errorListener.onError(new ResourceException(new IllegalArgumentException("Invalid credentials")));
                }
            }
        }, new OnErrorListener() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            @Override
            public void onError(Exception exception) {
//                User user = mEventDatabase.getUser(new User(username, password));
                User user = getCurrentUser((new User(username, password)));
                if (user != null) {
                    successListener.onSuccess("Offline authentication");
                } else {
                    errorListener.onError(new ResourceException(new IllegalArgumentException("Error occurred")));
                }
            }
        });
    }

    private void setCurrentUser(User user) {
        mCurrentUser = user;
        mEventRestClient.setUser(user);
    }

    public Cancellable saveEventAsync(final Event event,
                                      final OnSuccessListener<Event> onSuccessListener,
                                      final OnErrorListener onErrorListener) {
        return mEventRestClient.saveAsync(event, new OnSuccessListener<Event>() {
            @Override
            public void onSuccess(Event value) {
                if (value != null) {
                    Log.d(TAG, value.toString());
                    onSuccessListener.onSuccess(value);
                }
                onSuccessListener.onSuccess(new Event());
            }
        }, new OnErrorListener() {
            @Override
            public void onError(Exception exception) {
                event.setWhat(EventDatabase.ADD);
                event.setId("Added offline");
                mEventDatabase.saveEvent(event);
                onSuccessListener.onSuccess(new Event());
            }
        });
    }

    public Cancellable updateEventAsync(final Event event,
                                        final OnSuccessListener<Event> onSuccessListener,
                                        final OnErrorListener onErrorListener) {
        return mEventRestClient.updateAsync(event, new OnSuccessListener<Event>() {
            @Override
            public void onSuccess(Event value) {
                if (value != null) {
                    Log.d(TAG, value.toString());
                    onSuccessListener.onSuccess(value);
                }
                onSuccessListener.onSuccess(new Event());
            }
        }, new OnErrorListener() {
            @Override
            public void onError(Exception exception) {
                event.setWhat(EventDatabase.UPDATE);
                mEventDatabase.saveEvent(event);
                onSuccessListener.onSuccess(new Event());
            }
        });
    }

    public void subscribeChangeListener() {
        mEventSocketClient.subscribe(new ResourceChangeListener<Event>() {
            @Override
            public void onCreated(Event event) {
                Log.d(TAG, "changeListener, onCreated");
//                ensureNoteCached(note);
            }

            @Override
            public void onUpdated(Event event) {
                Log.d(TAG, "changeListener, onUpdated");
//                ensureNoteCached(note);
            }

            @Override
            public void onDeleted(String id) {
                Log.d(TAG, "changeListener, onDeleted");
//                if (mNotes.remove(noteId) != null) {
//                    setChanged();
//                    notifyObservers();
//                }
            }

            @Override
            public void onError(Throwable t) {
                Log.e(TAG, "changeListener, error", t);
            }
//            private void ensureNoteCached(Note note) {
//                if (!note.equals(mNotes.get(note.getId()))) {
//                    Log.d(TAG, "changeListener, cache updated");
//                    mNotes.put(note.getId(), note);
//                    setChanged();
//                    notifyObservers();
//                }
//            }
//
        });
    }

    public void unsubscribeChangeListener() {
        mEventSocketClient.unsubscribe();
    }

    public void setEventSocketClient(EventSocketClient eventSocketClient) {
        mEventSocketClient = eventSocketClient;
    }


    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public void synchronize(final OnSuccessListener<Event> onSuccessListener) {
        try {
            List<Event> events;
            events = mEventDatabase.getEvents(EventDatabase.ADD);
            if (events != null) {
                Log.d("ADD_EVENTS ", "NOT NULL");
                for (Event event : events) {
                    Log.d("ADD_EVENTS ", event.toString());
                    Event e = new Event();
                    e.setText(event.getText());
                    mEventRestClient.saveAsync(e, new OnSuccessListener<Event>() {
                        @Override
                        public void onSuccess(Event value) {

                        }
                    }, new OnErrorListener() {
                        @Override
                        public void onError(Exception exception) {

                        }
                    });
                }
            }
            events = mEventDatabase.getEvents(EventDatabase.UPDATE);
            if (events != null) {
                Log.d("UPDATE EVENTS ", "NOT NULL");
                for (Event event : events) {
                    mEventRestClient.updateAsync(event, new OnSuccessListener<Event>() {
                        @Override
                        public void onSuccess(Event value) {

                        }
                    }, new OnErrorListener() {
                        @Override
                        public void onError(Exception exception) {

                        }
                    });
                }
            }
            events = mEventDatabase.getEvents(EventDatabase.DELETE);
            if (events != null) {
                Log.d("DELETE EVENTS ", "NOT NULL");
                for (Event event : events) {
                    mEventRestClient.deleteAsync(event.getId(), new OnSuccessListener<Event>() {
                        @Override
                        public void onSuccess(Event value) {

                        }
                    }, new OnErrorListener() {
                        @Override
                        public void onError(Exception exception) {

                        }
                    });
                }
            }
        } catch (Exception ex) {
            Log.d("SYNCHRONIZE ", ex.toString());
        }
        onSuccessListener.onSuccess(new Event());
    }

//    private void updateCachedNotes(List<Note> notes) {
//        Log.d(TAG, "updateCachedNotes");
//        for (Note note : notes) {
//            mNotes.put(note.getId(), note);
//        }
//        setChanged();
//    }
//
//    private List<Note> cachedNotesByUpdated() {
//        ArrayList<Note> notes = new ArrayList<>(mNotes.values());
//        Collections.sort(notes, new NoteByUpdatedComparator());
//        return notes;
//    }
//
//    public List<Note> getCachedNotes() {
//        return cachedNotesByUpdated();
//    }

//    private class NoteByUpdatedComparator implements java.util.Comparator<Note> {
//        @Override
//        public int compare(Note n1, Note n2) {
//            return (int) (n1.getUpdated() - n2.getUpdated());
//        }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public User getCurrentUser(User u) {
        User user = mEventDatabase.getUser(u);
        setCurrentUser(user);
        mEventRestClient.setUser(user);
        return user;
    }
}
