package com.example.bogdan.eventmasterdetail.content.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.annotation.StringDef;
import android.util.Log;

import com.example.bogdan.eventmasterdetail.content.Event;
import com.example.bogdan.eventmasterdetail.content.User;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class EventDatabase extends SQLiteOpenHelper {
    public static final String ADD = "ADD";
    public static final String UPDATE = "UPDATE";
    public static final String DELETE = "DELETE";

    public static final String COLUMN_USERNAME = "username";
    public static final String COLUMN_PASSWORD = "cpassword";
    public static final String COLUMN_TOKEN = "token";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_USERID = "userid";
    public static final String COLUMN_TEXT = "ctext";
    public static final String COLUMN_UPDATED = "updated";
    public static final String COLUMN_VERSION = "version";
    public static final String COLUMN_WHAT = "what";

    public static final String TABLE_USERS = "users";
    public static final String TABLE_EVENTS = "events";

    public static final String DATABASE_NAME = "event.db";
    private static final int DATABASE_VERSION = 1;

    public EventDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d(EventDatabase.class.getSimpleName(), "on create database");
        db.execSQL("create table users(username text not null,\n" +
                "                  cpassword  text not null,\n" +
                "                  token text not null);");
        db.execSQL("create table events(id  text,\n" +
                "                    userid  text,\n" +
                "                    ctext text,\n" +
                "                    updated int,\n" +
                "                    version int,\n" +
                "                    what text);\n");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_USERS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_EVENTS);
        onCreate(db);
    }

    public void saveUser(User user) {
        Log.d("database saveUser", user.toString());
        ContentValues cv = new ContentValues();
        cv.put(COLUMN_USERNAME, user.getUsername());
        cv.put(COLUMN_PASSWORD, user.getPassword());
        cv.put(COLUMN_TOKEN, user.getToken());
        SQLiteDatabase db = getWritableDatabase();
        db.insert(TABLE_USERS, null, cv);
        db.close();
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public User getUser(User user) {
        final String select = "select username,cpassword,token from";
        Cursor c = getReadableDatabase().rawQuery(String.format("%s %s", select, TABLE_USERS), null, null);
        Log.d("database request", c.toString());
        c.moveToFirst();
        do {
            User u = new User(c.getString(0), c.getString(1));
            u.setToken(c.getString(2));
            if (user.equals(u)) {
                c.close();
                return u;
            }
        } while (c.moveToNext());
        c.close();
        return null;
    }

    public void saveEvent(Event event) {
        try {
            Log.d("database saveEvent", event.toString());
            ContentValues cv = new ContentValues();
            cv.put(COLUMN_ID, event.getId());
            cv.put(COLUMN_USERID, event.getUserId());
            cv.put(COLUMN_TEXT, event.getText());
            cv.put(COLUMN_UPDATED, event.getUpdated());
            cv.put(COLUMN_VERSION, event.getVersion());
            if (event.getWhat() != null) cv.put(COLUMN_WHAT, event.getWhat());
            SQLiteDatabase db = getWritableDatabase();
            if (event.getWhat().equals(UPDATE)) {
                db.update(TABLE_EVENTS, cv, "id =  ?" , new String[]{String.valueOf(event.getId())});
            } else {
                db.insert(TABLE_EVENTS, null, cv);
            }
            db.close();
        } catch (Exception ex) {
            Log.e("ERROR LA INSERT ", ex.toString());
        }
    }

    public void saveEvents(List<Event> events) {
        getWritableDatabase().delete(TABLE_EVENTS, null, null);
        for (Event event : events) {
            saveEvent(event);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public List<Event> getEvents() {
        final String select = "select id,userid,ctext,updated,version,what from events";
        Cursor c = getReadableDatabase().rawQuery(select, null, null);
        if (c.getCount() == 0) {
            c.close();
            return null;
        }
        Log.d("database request", c.toString());
        c.moveToFirst();
        List<Event> events = new ArrayList<>();
        do {
            Event event = new Event();
            event.setId(c.getString(0));
            event.setUserId(c.getString(1));
            event.setText(c.getString(2));
            event.setUpdated(c.getInt(3));
            event.setVersion(c.getInt(4));
            String what = c.getString(5);
            if (what != null && !what.equals(DELETE)) {
                events.add(event);
            }
        } while (c.moveToNext());
        c.close();
        return events.size() == 0 ? null : events;
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public List<Event> getEvents(String what) {
        try {
            final String select = "select id,userid,ctext,updated,version,what from events";
            Cursor c = getReadableDatabase().rawQuery(select, null, null);
            if (c.getCount() == 0) {
                c.close();
                return null;
            }
            Log.d("database request", c.toString());
            c.moveToFirst();
            List<Event> events = new ArrayList<>();
            do {
                Event event = new Event();
                event.setId(c.getString(0));
                event.setUserId(c.getString(1));
                event.setText(c.getString(2));
                event.setUpdated(c.getInt(3));
                event.setVersion(c.getInt(4));
                String w = c.getString(5);
                if (what.equals(w)) {
                    events.add(event);
                }
            } while (c.moveToNext());
            c.close();
            return events.size() == 0 ? null : events;
        } catch (Exception ex) {
            Log.e("ERROR IN GET EVENTS ", ex.toString());
            return null;
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    public Event getEvent(String id) {
        final String select = "select id,userid,ctext,updated,version,what from events where id = " + id;
        Cursor c = getReadableDatabase().rawQuery(select, null, null);
        Log.d("database request", c.toString());
        c.moveToFirst();
        Event event = new Event();
        event.setId(c.getString(0));
        event.setUserId(c.getString(1));
        event.setText(c.getString(2));
        event.setUpdated(c.getInt(3));
        event.setVersion(c.getInt(4));
        event.setWhat(c.getString(5));
        c.close();
        return event;
    }
}
