package com.example.bogdan.eventmasterdetail.net.mapping;

import android.util.JsonWriter;

import com.example.bogdan.eventmasterdetail.content.User;

import java.io.IOException;

import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Auth.USERNAME;
import static com.example.bogdan.eventmasterdetail.net.mapping.Api.Auth.PASSWORD;

public class CredentialsWriter implements ResourceWriter<User, JsonWriter> {

    @Override
    public void write(User user, JsonWriter jsonWriter) throws IOException {
        jsonWriter.beginObject();
        {
            jsonWriter.name(USERNAME).value(user.getUsername());
            jsonWriter.name(PASSWORD).value(user.getPassword());
        }
        jsonWriter.endObject();
    }
}
